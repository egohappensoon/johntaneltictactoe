package src;

import java.util.Arrays;
import java.util.Scanner;

public class TacTicToe {


    static String[] board;
    static String turnOfPlayer;
    static String winner = null;


    public static void main(String[] args) {
        board = new String[9];
        fillArray();
        turnOfPlayer = "X";
        System.out.println("Welcome to Game.");
        System.out.println("----------------");
        ticTacToe();

    }


    public static void ticTacToe() {
        winner = null;
        drawBoard();
        System.out.println(turnOfPlayer + "'s choice. Enter a place for " + turnOfPlayer);
        try {
            Scanner userInput = new Scanner(System.in);
            int placement = userInput.nextInt();
            String fill = turnOfPlayer;
            //The game fills the spots based on whose turn is it
            if (board[placement].equals("X") || board[placement].equals("O")) {
                System.out.println("This spot has already been chosen, please choose a different spot");
                ticTacToe();
                //if the spot is taken, the game will start over, but the board remains as it was
            } else {
                board[placement] = fill;
                //if not, it places the players symbol on the wanted spot
            }
        } catch (Exception e) {
            System.out.println("Please choose a number on the board");
            ticTacToe();
            //any exception we get (if the user enters a letter or a number that is not on the board, etc.)
            //we just print out the message and start the game again with the board the same way as it was
        }
        //and this is to switch players
        if (turnOfPlayer.equals("X")) {
            turnOfPlayer = "O";
        } else {
            turnOfPlayer = "X";
        }
        winningText();      // Calling the winningText() method to check for a winner or tie after a player plays
        ticTacToe();

    }


    //In this method, checkWinner is called to see if any player won or it's a tie
    static String winningText() {
        winner = checkWinner();
        if (winner != null) {
            //check if the players have played for 7 times
            if (turnOfPlayer.equals(7)) {
                System.out.println("Sorry, the game is a tie!");
            } else {
                //compares the winning combo to that of checkWinner(), if they match it prints them out and terminate the jvm
                if (winner.equalsIgnoreCase("XXX") || winner.equalsIgnoreCase("OOO")){
                }
                System.out.println("Congratulations! " + winner + " is the winning combination!");
                System.exit(0); //Terminates jvm thereby ending the game
            }
        }
        return null;
    }

    //Check the board for a winner, else it is a tie. It returns the values corresponding to the winning combos.
    static String checkWinner() {
        for (int l = 0; l < 8; l++) {
            String line = null;
            switch (l) {
                case 0:
                    line = board[0] + board[1] + board[2];
                    break;
                case 1:
                    line = board[3] + board[4] + board[5];
                    break;
                case 2:
                    line = board[6] + board[7] + board[8];
                    break;
                case 3:
                    line = board[0] + board[3] + board[6];
                    break;
                case 4:
                    line = board[1] + board[4] + board[7];
                    break;
                case 5:
                    line = board[2] + board[5] + board[8];
                    break;
                case 6:
                    line = board[0] + board[4] + board[8];
                    break;
                case 7:
                    line = board[2] + board[4] + board[6];
                    break;
            }
            if (line.equals("XXX")) {
                System.out.println("Player 'X' won the game!");
                return "XXX";
            } else if (line.equals("OOO")) {
                System.out.println("Player 'O' won the game!");
                return "OOO";
            }
        }
        return null;
    }


    public static void fillArray() {
        for (int i = 0; i < 9; i++) {
            board[i] = String.valueOf(i);
        }
    }

    public static void drawBoard() {

        System.out.println("-------------");
        System.out.println("| " + board[0] + " | " + board[1] + " | " + board[2] + " |");
        System.out.println("-------------");
        System.out.println("| " + board[3] + " | " + board[4] + " | " + board[5] + " |");
        System.out.println("-------------");
        System.out.println("| " + board[6] + " | "  + board[7] + " | " + board[8] + " |");
        System.out.println("-------------");
    }

}






